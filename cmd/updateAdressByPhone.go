package cmd

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	pb "gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/model"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// апдейт поля адрес в существующей записи по номеру телефона
var updateAdressByPhoneCmd = &cobra.Command{
	Use: "updateAdressByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(pb.Record)
		fmt.Print("Для изменения имени записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Number)
		in, _ := ioutil.ReadFile(nameCreatedFile)
		book := &pb.PhoneBook{}
		if err := proto.Unmarshal(in, book); err != nil {
			log.Fatalln("Failed to parse address book:", err)
		}
		for _, row := range book.Record {
			if row.Number == r.Number {
				fmt.Println("Введите адрес в формате: город улица дом квартира: ")
				myscanner := bufio.NewScanner(os.Stdin)
				myscanner.Scan()
				adr := strings.Split(myscanner.Text(), " ")
				if len(adr) != 4 {
					fmt.Println("Aдрес введен неверно")
				} else {
					row.Adress = &pb.Record_Adress{
						Sity:       adr[0],
						Street:     adr[1],
						Building:   adr[2],
						Appartment: adr[3],
					}

					out, err := proto.Marshal(book)
					if err != nil {
						log.Fatalln("Failed to encode address book:", err)
					}
					if err := ioutil.WriteFile(nameCreatedFile, out, 0644); err != nil {
						log.Fatalln("Failed to write address book:", err)
					}
				}
				return
			}
		}
		fmt.Println("Искомая запись не найдена")

	},
}

func init() {
	rootCmd.AddCommand(updateAdressByPhoneCmd)
}
