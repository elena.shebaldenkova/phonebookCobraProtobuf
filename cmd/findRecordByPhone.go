package cmd

import (
	"fmt"
	"io/ioutil"
	"log"

	pb "gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/model"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// найти запись по номеру телефона
var findRecordByPhoneCmd = &cobra.Command{
	Use: "findRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(pb.Record)
		fmt.Print("Для добавления новой записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Number)
		in, _ := ioutil.ReadFile(nameCreatedFile)
		book := &pb.PhoneBook{}
		if err := proto.Unmarshal(in, book); err != nil {
			log.Fatalln("Failed to parse address book:", err)
		}
		for _, row := range book.Record {
			if row.Number == r.Number {
				fmt.Println(" Number: " + row.Number + " Name: " + row.Name + " Adress: " + " City: " + row.Adress.GetSity() + " Street: " + row.Adress.GetStreet() + " Building: " + row.Adress.GetBuilding() + " Appartment: " + row.Adress.GetAppartment())
				return
			}
		}

		fmt.Println("Искомая запись отсутствует")

	},
}

func init() {
	rootCmd.AddCommand(findRecordByPhoneCmd)
}
