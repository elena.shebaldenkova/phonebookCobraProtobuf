package cmd

import (
	"fmt"
	"io/ioutil"
	"log"

	pb "gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/model"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// вывод всех записей из телефонной книги
var viewAllRecordsCmd = &cobra.Command{
	Use: "viewAllRecords",
	Run: func(cmd *cobra.Command, args []string) {

		in, _ := ioutil.ReadFile(nameCreatedFile)
		book := &pb.PhoneBook{}
		if err := proto.Unmarshal(in, book); err != nil {
			log.Fatalln("Failed to parse address book:", err)
		}
		for _, row := range book.Record {
			//fmt.Println(proto.CompactTextString(row)) /*вывод каждой записи из тел книги, если записи на английском*/
			//этот формат вывода использован для получения и вывода значений на русском языке
			fmt.Println(" Number: " + row.Number + " Name: " + row.Name + " Adress: " + " City: " + row.Adress.GetSity() + " Street: " + row.Adress.GetStreet() + " Building: " + row.Adress.GetBuilding() + " Appartment: " + row.Adress.GetAppartment())

		}
	},
}

func init() {
	rootCmd.AddCommand(viewAllRecordsCmd)
}
