package cmd

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	pb "gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/model"
	"gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/utils"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

var nameCreatedFile = "MyPhoneBook.txt"

// добавление новой записи в тел книгу
var addRecordByPhoneCmd = &cobra.Command{
	Use: "addRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(pb.Record)
		fmt.Print("Для добавления новой записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Number)
		if !utils.CheckValidNumber(r.Number) {
			fmt.Println("Введенный номер не соответствует требуемому формату")
			return
		}
		if !checkAvialableNumberInFile(r.Number) {
			fmt.Print("Введите Имя: ")
			fmt.Scanln(&r.Name)
			fmt.Print("Введите адрес в формате: город улица дом квартира: ")
			myscanner := bufio.NewScanner(os.Stdin)
			myscanner.Scan()
			adr := strings.Split(myscanner.Text(), " ")
			if len(adr) != 4 {
				fmt.Println("Aдрес введен неверно")
			} else {

				r := &pb.Record{
					Number: r.Number,
					Name:   r.Name,
					Adress: &pb.Record_Adress{
						Sity:       adr[0],
						Street:     adr[1],
						Building:   adr[2],
						Appartment: adr[3],
					}}
				phonebook := new(pb.PhoneBook)
				phonebook.Record = append(phonebook.Record, r)
				out, err := proto.Marshal(phonebook)
				if err != nil {
					log.Fatalln("Failed to encode address book:", err)
				}
				f, err := os.OpenFile(nameCreatedFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600) //открытие файла. Если нет, тогда создаем новый
				if err != nil {
					panic(err)
				}

				defer f.Close()
				if _, err = f.WriteString(string(out)); err != nil {
					panic(err)
				}
			}
		} else {
			fmt.Println("Запись уже существует")
		}
	},
}

func init() {
	rootCmd.AddCommand(addRecordByPhoneCmd)
}
