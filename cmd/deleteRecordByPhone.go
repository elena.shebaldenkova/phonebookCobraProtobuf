package cmd

import (
	"fmt"
	"io/ioutil"
	"log"

	pb "gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git/model"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
)

// удаление записи по номеру телефона
var deleteRecordByPhoneCmd = &cobra.Command{
	Use: "deleteRecordByPhone",
	Run: func(cmd *cobra.Command, args []string) {
		r := new(pb.Record)
		fmt.Print("Для поиска и удаления записи введите номер телефона в формате +380501234567: ")
		fmt.Scanln(&r.Number)
		in, _ := ioutil.ReadFile(nameCreatedFile)
		book := &pb.PhoneBook{}
		if err := proto.Unmarshal(in, book); err != nil {
			log.Fatalln("Failed to parse address book:", err)
		}
		for i, row := range book.Record {
			if row.Number == r.Number {
				lastIndex := len(book.Record) - 1
				book.Record[i], book.Record[lastIndex] = book.Record[lastIndex], book.Record[i]
				book.Record = book.Record[:lastIndex]
				fmt.Println("Запись по номеру " + r.Number + " удалена успешно")
				out, err := proto.Marshal(book)
				if err != nil {
					log.Fatalln("Failed to encode address book:", err)
				}
				if err := ioutil.WriteFile(nameCreatedFile, out, 0644); err != nil {
					log.Fatalln("Failed to write address book:", err)
				}
				return
			}
		}
		fmt.Println("Номер " + r.Number + " не найден в базе")

	},
}

func init() {
	rootCmd.AddCommand(deleteRecordByPhoneCmd)
}
