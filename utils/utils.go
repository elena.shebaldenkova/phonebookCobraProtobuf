package utils

import (
	"regexp"
)

func CheckValidNumber(key string) bool {
	var validNumber = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`)
	val := validNumber.MatchString(key)
	return val
}
