***Создание проекта***
1. Создаем проект с помощью кобры - новый
2. Выполните следующую команду для установки плагина protobuf протокола Go:
 go get github.com/golang/protobuf/protoc-gen-go
3. Создайте proto файл в пакете model
 Например: record.proto 
 внутри будет содержать следущее:

 
  syntax = "proto3";
  
package model;

message Record {

    string number = 1;
    
    string name = 2;
    
    string adress = 3;
    
   }


4. Далее нужен компилятор protobuf, для этого:
 - перейдите по ссылке https://github.com/google/protobuf/releases
 - скачайте zip архив protoc-3.5.0-win32.zip
 - распакуйте и положите эту папку в $GOPATH/bin
5. В консоле перейдите в $GOPATH/bin/protoc-3.5.0-win32/bin
6. Далее из этой директории запустите команду:
 protoc -I=$GOPATH\src\[имя проекта] --go_out=$GOPATH\src\[имя проекта] $GOPATH\src\[имя проекта]\model\record.proto (после выполнения у вас в директории $GOPATH\src\[имя проекта]\model сгенерится структура record.pb.go)
7. Перейдите опять в каталог $GOPATH\src\[имя проекта]
8. Создайте в каталоге проекта с помощью кобры команду -> cobra add 
9. Запустите  go run main.go addRecordByPhone


***Работа с прoектом***
Скачать проект - go get gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git

Перейти в терминале в папку  %GOPATH%/src/gitlab.com/elena.shebaldenkova/phonebookCobraProtobuf.git

Выполнить команду  -  go run main.go [выбранную команду], в зависимости от желаемых действий:

***Перечень доступных команд*** 

 - addRecordByPhone - добавить запись в телефонную книгу по номеру телефона 

 - viewAllRecord - просмотр всех записей из телефонной книги 

 - findRecordByPhone - найти запись в телефонной книге по номеру телефона

 - deleteRecordByPhone - удалить запись из телефонной книги по номеру телефона

 - updateNameByPhone - обновить имя в телефонной книге по номеру телефона 

 - updateAdressByPhone - обновить адрес в телефонной книге по номеру телефона

 